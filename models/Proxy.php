<?php
class Proxy
{
    /**
     * @param $post
     *
     * add new post in DB
     */
    public function add($post)
    {

        $pdo = new PDO('mysql:host=' . 'localhost' . ';dbname=' . 'gbook', 'root', '');

        $query = $pdo->prepare("INSERT INTO gbook.msgs (name, email, msg)
                                VALUES (:name, :email, :msg)");
        $query -> bindParam(':name', $post->name);
        $query -> bindParam(':email', $post->email);
        $query -> bindParam(':msg', $post->content);

        $query -> execute();

        $pdo = null;
    }

    /**
     * @return array
     *
     * return all posts
     */
    public function getAll()
    {
        $pdo = new PDO('mysql:host=' . 'localhost' . ';dbname=' . 'gbook', 'root', '');

        $sql = 'SELECT * FROM gbook.msgs ORDER BY id DESC';
        $query = $pdo->query($sql);
        $results = $query->fetchAll(\PDO::FETCH_ASSOC);

        $pdo = null;

        return $results;
    }

    /**
     * @param $id
     * @return array
     *
     * delete post where id= $id
     */
    public function delete($id)
    {
        $pdo = new PDO('mysql:host=' . 'localhost' . ';dbname=' . 'gbook', 'root', '');

        $sql = 'DELETE FROM gbook.msgs WHERE id =' . $id ;
        $query = $pdo->query($sql);
        $results = $query->fetchAll(\PDO::FETCH_ASSOC);

        $pdo = null;

        return $results;
    }

    /**
     * @param $i
     * @return array
     *
     * return post where id = $id
     */
    public function get($id)
    {
        $pdo = new PDO('mysql:host=' . 'localhost' . ';dbname=' . 'gbook', 'root', '');

        $sql = 'SELECT * FROM gbook.msgs WHERE id =' . $id ;
        $query = $pdo->query($sql);
        $results = $query->fetchAll(\PDO::FETCH_ASSOC);

        $pdo = null;

        return $results;
    }
}